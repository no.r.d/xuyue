"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
from nord.sigurd.shell.visnode import VisNode
from nord.sigurd.utils.verboser import funlog


class basevis(VisNode):
    """Implement the visualization."""

    def __init__(self, uid, app):
        """Wrap the parent class constructor."""
        super().__init__(uid, self.__class__.__name__.lower(), app, space="xuyue")

    @funlog
    def update_tooltip(self, tooltip):
        if hasattr(self, 'mapentry'):
            txt = self.mapentry.vis_repr()
        else:
            txt = _(f"Operator: {self.__class__.__name__}")  # noqa: F821
        tooltip.set_text(txt)
