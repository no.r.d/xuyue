"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Receiver as Receiver
import nord.nord.exceptions.RuntimeException as RuntimeException
import sys


class Operator(Receiver):
    def _initialize(self, val):
        self.value = val

    def _op_assign(self, val):
        """
        This should be overridden by all xuyue nodes with
        their specific operation and self assignment function.
        """
        raise RuntimeException(_("{} _op_assign not implemented").format(self.__class__.__name__))  # noqa: F821

    def execute(self, runtime):
        args, kwargs = self.get_arg_values()
        self.value = None
        for i in args:
            if self.value is None:
                self._initialize(i)
            else:
                self._op_assign(i)

        # By setting this variable whose name does not start with a '_'
        # we are taking advantage of the CDC system on the node
        # to synchronize this value with the subscribers. This probably
        # should only be done when the map is being debugged...
        if runtime.debug:
            self.vis_exe_repr = self._str_equiv.join([str(x) for x in args])

        self.flag_exe(runtime)

    def get(self):
        if hasattr(self, 'value'):
            return self.value
        else:
            raise RuntimeException(_("xuyue operator {} passed no arguments").format(self.__class__.__name__))  # noqa: F821, E501

    def vis_repr(self):
        if hasattr(self, 'vis_exe_repr'):
            return self.vis_exe_repr
        else:
            return self._str_equiv.join([str(x.get_source().__class__.__name__) for x in self.get_sources('pass')])

sys.modules[__name__] = Operator
