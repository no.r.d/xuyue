"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
Enable passing of arguments to the And such that it can be
executed.
"""
import nord.nord.Rule as Rule
from nord.nord.exceptions import RuntimeException as RuntimeException
import sys


class BasePass(Rule):
    def apply(self):
        """
        Takes the cargo off the edge and adds it to the target builtin's
        set of arguments.
        """
        if self.edge.has_cargo():
            self.edge.get_target().set_arg(self.edge, self.edge.get_cargo())
            return True
        else:
            raise RuntimeException(_("{} target of pass, but nothing passed: {}").format(  # noqa: F821
                    self.__class__.__name__,
                    self.edge.get_target()))


sys.modules[__name__] = BasePass
