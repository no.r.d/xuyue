"""
Copyright 2021 NoRD Software Foundation

This file is part of NoRD.

NoRD is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NoRD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with NoRD.  If not, see <https://www.gnu.org/licenses/>.

--
"""
import nord.nord.Runtime as RT
import nord.nord.Map as Map
import json
# import pytest
Runtime = RT.Runtime


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_and_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is False

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticbool',
        'value': 'False'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is False

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v2').set_property('value', 'True')
    karte.get_node('v3').set_property('value', 'True')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v1').set_property('value', 'True')
    karte.get_node('v2').set_property('value', 'True')
    karte.get_node('v3').set_property('value', 'True')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_minus_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "minus"

    m["map"]["nodes"][2]["type"] = "staticint"
    m["map"]["nodes"][2]["value"] = "44"

    m["map"]["nodes"][3]["type"] = "staticint"
    m["map"]["nodes"][3]["value"] = "43"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 1

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticfloat',
        'value': '1.5'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == -0.5


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_plus_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "plus"

    m["map"]["nodes"][2]["type"] = "staticint"
    m["map"]["nodes"][2]["value"] = "44"

    m["map"]["nodes"][3]["type"] = "staticint"
    m["map"]["nodes"][3]["value"] = "43"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 87

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticfloat',
        'value': '1.5'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 88.5


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_times_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "times"

    m["map"]["nodes"][2]["type"] = "staticint"
    m["map"]["nodes"][2]["value"] = "4"

    m["map"]["nodes"][3]["type"] = "staticint"
    m["map"]["nodes"][3]["value"] = "3"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 12

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticfloat',
        'value': '1.5'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 18.0


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_over_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "over"

    m["map"]["nodes"][2]["type"] = "staticint"
    m["map"]["nodes"][2]["value"] = "44"

    m["map"]["nodes"][3]["type"] = "staticint"
    m["map"]["nodes"][3]["value"] = "4"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 11

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticfloat',
        'value': '1.5'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 7.333333333333333


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_tothe_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "tothe"

    m["map"]["nodes"][2]["type"] = "staticint"
    m["map"]["nodes"][2]["value"] = "2"

    m["map"]["nodes"][3]["type"] = "staticint"
    m["map"]["nodes"][3]["value"] = "4"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 16

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticfloat',
        'value': '1.5'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value == 64.0


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_xor_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "xor"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticbool',
        'value': 'False'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v3').set_property('value', 'True')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is False

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v1').set_property('value', 'True')
    karte.get_node('v2').set_property('value', 'True')
    karte.get_node('v3').set_property('value', 'True')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_or_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "or"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticbool',
        'value': 'False'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v2').set_property('value', 'True')
    karte.get_node('v3').set_property('value', 'True')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v1').set_property('value', 'False')
    karte.get_node('v2').set_property('value', 'False')
    karte.get_node('v3').set_property('value', 'False')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is False


# @pytest.mark.skip(reason="THIS TEST ENDS UP BLOCKING WHEN RUN IN BATCH")
def test_execute_not_op():
    try:
        m = json.load(open('tests/maps/test_operators.n', 'r'))
    except FileNotFoundError:
        m = json.load(open('xuyue/tests/maps/test_operators.n', 'r'))

    m["map"]["nodes"][0]["type"] = "not"

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    m["map"]["nodes"].append({
        'id': 'v3',
        'name': 'v3',
        'space': 'sushrut',
        'type': 'staticbool',
        'value': 'False'
    })

    m["map"]["edges"].append({
        "from_space": "sushrut",
        "rule": "pass",
        'source': 'v3:v3',
        'to_space': 'xuyue',
        'target': 'Operator:Operator'
    })

    karte = Map(m, verbose=True)

    vessal = Runtime()
    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is True

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v2').set_property('value', 'True')
    karte.get_node('v3').set_property('value', 'True')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is False

    karte = Map(m, verbose=True)
    vessal = Runtime()
    karte.get_node('v1').set_property('value', 'True')
    karte.get_node('v2').set_property('value', 'True')
    karte.get_node('v3').set_property('value', 'True')

    karte.run(vessal, None)
    assert vessal.get_exec_count() == 1
    assert karte.nodes['result'].value is False
