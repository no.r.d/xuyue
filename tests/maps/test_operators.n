{
    "extensions": {
        "athena": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/athena/src/nord/athena",
                    "pkg://"
                ]
            }
        },
        "enrique": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/enrique/src/nord/enrique",
                    "pkg://"
                ]
            }
        },
        "fregata": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir://../xuyue/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ],
                "path": [
                    "dir://../xuyue/tests/maps",
                    "dir:///Users/nate/Desktop/Dev/NoRD/fregata/src/nord/fregata",
                    "pkg://"
                ]
            }
        },
        "iteru": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/iteru/src/nord/iteru",
                    "pkg://"
                ]
            }
        },
        "jabir": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/jabir/src/nord/jabir",
                    "pkg://"
                ]
            }
        },
        "kamal": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/kamal/src/nord/kamal",
                    "pkg://"
                ]
            }
        },
        "pasion": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/pasion/src/nord/pasion",
                    "pkg://"
                ]
            }
        },
        "shoshoni": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/shoshoni/src/nord/shoshoni",
                    "pkg://"
                ]
            }
        },
        "sushrut": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/sushrut/src/nord/sushrut",
                    "pkg://"
                ]
            }
        },
        "wrigan": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/wrigan/src/nord/wrigan",
                    "pkg://"
                ]
            }
        },
        "xuyue": {
            "locations": {
                "code": [
                    "null://."
                ],
                "files": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ],
                "path": [
                    "dir:///Users/nate/Desktop/Dev/NoRD/xuyue/src/nord/xuyue",
                    "pkg://"
                ]
            }
        }
    },
    "map": {
        "edges": [
            {
                "from_space": "xuyue",
                "rule": "assign",
                "source": "Operator:Operator",
                "space": "nord",
                "target": "result:result",
                "to_space": "sushrut"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "v1:v1",
                "space": "nord",
                "target": "Operator:Operator",
                "to_space": "xuyue"
            },
            {
                "from_space": "sushrut",
                "rule": "pass",
                "source": "v2:v2",
                "space": "nord",
                "target": "Operator:Operator",
                "to_space": "xuyue"
            }
        ],
        "nodes": [
            {
                "id": "Operator",
                "name": "Operator",
                "position": [
                    2.4024925231933594,
                    0.6232986450195312,
                    0.01275255810469389
                ],
                "space": "xuyue",
                "type": "and"
            },
            {
                "id": "result",
                "name": "result",
                "position": [
                    10.702766418457031,
                    -1.3438682556152344,
                    0.033007584512233734
                ],
                "space": "nord",
                "type": "data"
            },
            {
                "id": "v1",
                "name": "v1",
                "position": [
                    -8.215897560119629,
                    -0.09199142456054688,
                    4.195023536682129
                ],
                "space": "sushrut",
                "type": "staticbool",
                "value": "True"
            },
            {
                "id": "v2",
                "name": "v2",
                "position": [
                    -7.825085639953613,
                    7.62939453125e-06,
                    -3.8297078609466553
                ],
                "space": "sushrut",
                "type": "staticbool",
                "value": "False"
            }
        ]
    },
    "name": "Soggy Chipped Bouy"
}