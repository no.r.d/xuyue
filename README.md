Named for Xu Yue the author of a Han Dynasty book which describes an early Chinese abacus. This extension provides arithmatic and other operators to NoRD.

See (https://en.wikipedia.org/wiki/Suanpan) for more background about the naming.

